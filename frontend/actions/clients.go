package actions

//ServerConnected is the action to change the connection status of the web server
type ServerConnected struct {
	Connected bool
}
